# GivingTheme

Template Wordpress Giving Theme

Este template solo debe instalarse en un Wordpress, cuenta con una vista para el home.

Al instalar el tema es indispensable seleccionar en el apartado Ajustes>Lectura>Una página estática. Esto para poder asignar de forma automática al Home este tema.

Al instalar crea un custom post llamado Portafolio, se pueden incluír diferentes items en el menú portafolio igual que un post.
Al crear un post de portafolio, se puede elegir una imagen y esta es la qeu mostrará en el front.

Para que se puedan visualizar, se debe seleccionar en el apartado de Widgets de wordpress un widget ed tipo "Widget Portafolio" y asignarlo a Giving Sidebar Posición para que este muestre en el home.

